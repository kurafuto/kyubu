// Package "minimal" (actually "modern" for compatibility reasons) implements a
// subset of the Minecraft protocol. It basically contains enough for Kurafuto
// to work, as well as a generic packet that acts as a catch-all and just
// throws around bytes.
package modern
