// Generated by protocol_generator; DO NOT EDIT
// protocol_generator -file=play_clientbound.go -direction=clientbound -state=play -package=modern

package modern

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/kurafuto/kyubu/packets"
	"io"
	"io/ioutil"
)

func init() {
	packets.Register(packets.Play, packets.ClientBound, 0x01, func() packets.Packet { return &JoinGame{} })
	packets.Register(packets.Play, packets.ClientBound, 0x02, func() packets.Packet { return &ServerMessage{} })
	packets.Register(packets.Play, packets.ClientBound, 0x07, func() packets.Packet { return &Respawn{} })
	packets.Register(packets.Play, packets.ClientBound, 0x38, func() packets.Packet { return &PlayerListItem{} })
	packets.Register(packets.Play, packets.ClientBound, 0x3a, func() packets.Packet { return &ServerTabComplete{} })
	packets.Register(packets.Play, packets.ClientBound, 0x3b, func() packets.Packet { return &ScoreboardObjective{} })
	packets.Register(packets.Play, packets.ClientBound, 0x3c, func() packets.Packet { return &UpdateScore{} })
	packets.Register(packets.Play, packets.ClientBound, 0x3d, func() packets.Packet { return &ShowScoreboard{} })
	packets.Register(packets.Play, packets.ClientBound, 0x3e, func() packets.Packet { return &Teams{} })
	packets.Register(packets.Play, packets.ClientBound, 0x3f, func() packets.Packet { return &ServerPluginMessage{} })
	packets.Register(packets.Play, packets.ClientBound, 0x40, func() packets.Packet { return &Disconnect{} })
	packets.Register(packets.Play, packets.ClientBound, 0x46, func() packets.Packet { return &SetCompression{} })
}

func (t *JoinGame) Id() byte {
	return 0x01 // 1
}

func (t *JoinGame) Encode(ww io.Writer) (err error) {
	if err = binary.Write(ww, binary.BigEndian, t.EntityID); err != nil {
		return err
	}

	if err = binary.Write(ww, binary.BigEndian, t.Gamemode); err != nil {
		return err
	}

	if err = binary.Write(ww, binary.BigEndian, t.Dimension); err != nil {
		return err
	}

	if err = binary.Write(ww, binary.BigEndian, t.Difficulty); err != nil {
		return err
	}

	if err = binary.Write(ww, binary.BigEndian, t.MaxPlayers); err != nil {
		return err
	}

	tmp0 := make([]byte, binary.MaxVarintLen32)
	tmp1 := []byte(t.LevelType)
	tmp2 := packets.PutVarint(tmp0, int64(len(tmp1)))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp2]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp1); err != nil {
		return err
	}

	tmp3 := byte(0)
	if t.ReducedDebug {
		tmp3 = byte(1)
	}
	if err = binary.Write(ww, binary.BigEndian, tmp3); err != nil {
		return err
	}

	return
}

func (t *JoinGame) Decode(rr io.Reader) (err error) {
	if err = binary.Read(rr, binary.BigEndian, t.EntityID); err != nil {
		return err
	}

	if err = binary.Read(rr, binary.BigEndian, t.Gamemode); err != nil {
		return err
	}

	if err = binary.Read(rr, binary.BigEndian, t.Dimension); err != nil {
		return err
	}

	if err = binary.Read(rr, binary.BigEndian, t.Difficulty); err != nil {
		return err
	}

	if err = binary.Read(rr, binary.BigEndian, t.MaxPlayers); err != nil {
		return err
	}

	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp1 := make([]byte, tmp0)
	tmp2, err := rr.Read(tmp1)
	if err != nil {
		return err
	} else if int64(tmp2) != tmp0 {
		return errors.New("didn't read enough bytes for string")
	}
	t.LevelType = string(tmp1)

	var tmp3 [1]byte
	if _, err = rr.Read(tmp3[:1]); err != nil {
		return err
	}
	t.ReducedDebug = tmp3[0] == 0x01

	return
}

func (t *ServerMessage) Id() byte {
	return 0x02 // 2
}

func (t *ServerMessage) Encode(ww io.Writer) (err error) {
	var tmp0 []byte
	if tmp0, err = json.Marshal(&t.Data); err != nil {
		return err
	}
	if err = packets.WriteString(ww, string(tmp0)); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, t.Position); err != nil {
		return err
	}

	return
}

func (t *ServerMessage) Decode(rr io.Reader) (err error) {
	var tmp0 string
	if tmp0, err = packets.ReadString(rr); err != nil {
		return err
	}
	if err = json.Unmarshal([]byte(tmp0), &t.Data); err != nil {
		return err
	}
	if err = binary.Read(rr, binary.BigEndian, t.Position); err != nil {
		return err
	}

	return
}

func (t *Respawn) Id() byte {
	return 0x07 // 7
}

func (t *Respawn) Encode(ww io.Writer) (err error) {
	if err = binary.Write(ww, binary.BigEndian, t.Dimension); err != nil {
		return err
	}

	if err = binary.Write(ww, binary.BigEndian, t.Difficulty); err != nil {
		return err
	}

	if err = binary.Write(ww, binary.BigEndian, t.Gamemode); err != nil {
		return err
	}

	tmp0 := make([]byte, binary.MaxVarintLen32)
	tmp1 := []byte(t.LevelType)
	tmp2 := packets.PutVarint(tmp0, int64(len(tmp1)))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp2]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp1); err != nil {
		return err
	}

	return
}

func (t *Respawn) Decode(rr io.Reader) (err error) {
	if err = binary.Read(rr, binary.BigEndian, t.Dimension); err != nil {
		return err
	}

	if err = binary.Read(rr, binary.BigEndian, t.Difficulty); err != nil {
		return err
	}

	if err = binary.Read(rr, binary.BigEndian, t.Gamemode); err != nil {
		return err
	}

	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp1 := make([]byte, tmp0)
	tmp2, err := rr.Read(tmp1)
	if err != nil {
		return err
	} else if int64(tmp2) != tmp0 {
		return errors.New("didn't read enough bytes for string")
	}
	t.LevelType = string(tmp1)

	return
}

func (t *PlayerListItem) Id() byte {
	return 0x38 // 56
}

func (t *PlayerListItem) Encode(ww io.Writer) (err error) {
	tmp0 := make([]byte, binary.MaxVarintLen64)
	tmp1 := packets.PutVarint(tmp0, int64(t.Action))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp1]); err != nil {
		return err
	}

	tmp2 := make([]byte, binary.MaxVarintLen64)
	tmp3 := packets.PutVarint(tmp2, int64(t.NumPlayers))
	if err = binary.Write(ww, binary.BigEndian, tmp2[:tmp3]); err != nil {
		return err
	}

	tmp4 := make([]byte, binary.MaxVarintLen64)
	tmp5 := packets.PutVarint(tmp4, int64(len(t.Players)))
	if err = binary.Write(ww, binary.BigEndian, tmp4[:tmp5]); err != nil {
		return err
	}

	for tmp6 := range t.Players {
		if err = binary.Write(ww, binary.BigEndian, t.Players[tmp6].UUID[:]); err != nil {
			return err
		}

		tmp7 := make([]byte, binary.MaxVarintLen32)
		tmp8 := []byte(t.Players[tmp6].Name)
		tmp9 := packets.PutVarint(tmp7, int64(len(tmp8)))
		if err = binary.Write(ww, binary.BigEndian, tmp7[:tmp9]); err != nil {
			return err
		}
		if err = binary.Write(ww, binary.BigEndian, tmp8); err != nil {
			return err
		}

		tmp10 := make([]byte, binary.MaxVarintLen64)
		tmp11 := packets.PutVarint(tmp10, int64(len(t.Players[tmp6].Properties)))
		if err = binary.Write(ww, binary.BigEndian, tmp10[:tmp11]); err != nil {
			return err
		}

		for tmp12 := range t.Players[tmp6].Properties {
			tmp13 := make([]byte, binary.MaxVarintLen32)
			tmp14 := []byte(t.Players[tmp6].Properties[tmp12].Name)
			tmp15 := packets.PutVarint(tmp13, int64(len(tmp14)))
			if err = binary.Write(ww, binary.BigEndian, tmp13[:tmp15]); err != nil {
				return err
			}
			if err = binary.Write(ww, binary.BigEndian, tmp14); err != nil {
				return err
			}

			tmp16 := make([]byte, binary.MaxVarintLen32)
			tmp17 := []byte(t.Players[tmp6].Properties[tmp12].Value)
			tmp18 := packets.PutVarint(tmp16, int64(len(tmp17)))
			if err = binary.Write(ww, binary.BigEndian, tmp16[:tmp18]); err != nil {
				return err
			}
			if err = binary.Write(ww, binary.BigEndian, tmp17); err != nil {
				return err
			}

			tmp19 := byte(0)
			if t.Players[tmp6].Properties[tmp12].Signed {
				tmp19 = byte(1)
			}
			if err = binary.Write(ww, binary.BigEndian, tmp19); err != nil {
				return err
			}

			if t.Players[tmp6].Properties[tmp12].Signed {
				tmp20 := make([]byte, binary.MaxVarintLen32)
				tmp21 := []byte(t.Players[tmp6].Properties[tmp12].Signature)
				tmp22 := packets.PutVarint(tmp20, int64(len(tmp21)))
				if err = binary.Write(ww, binary.BigEndian, tmp20[:tmp22]); err != nil {
					return err
				}
				if err = binary.Write(ww, binary.BigEndian, tmp21); err != nil {
					return err
				}

			}
		}
		tmp23 := make([]byte, binary.MaxVarintLen64)
		tmp24 := packets.PutVarint(tmp23, int64(t.Players[tmp6].Gamemode))
		if err = binary.Write(ww, binary.BigEndian, tmp23[:tmp24]); err != nil {
			return err
		}

		tmp25 := make([]byte, binary.MaxVarintLen64)
		tmp26 := packets.PutVarint(tmp25, int64(t.Players[tmp6].Ping))
		if err = binary.Write(ww, binary.BigEndian, tmp25[:tmp26]); err != nil {
			return err
		}

		tmp27 := byte(0)
		if t.Players[tmp6].HasDisplayName {
			tmp27 = byte(1)
		}
		if err = binary.Write(ww, binary.BigEndian, tmp27); err != nil {
			return err
		}

		if t.Players[tmp6].HasDisplayName {
			var tmp28 []byte
			if tmp28, err = json.Marshal(&t.Players[tmp6].DisplayName); err != nil {
				return err
			}
			if err = packets.WriteString(ww, string(tmp28)); err != nil {
				return err
			}
		}
	}
	return
}

func (t *PlayerListItem) Decode(rr io.Reader) (err error) {
	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	t.Action = packets.VarInt(tmp0)

	tmp1, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	t.NumPlayers = packets.VarInt(tmp1)

	var tmp2 packets.VarInt
	tmp3, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp2 = packets.VarInt(tmp3)

	if tmp2 < 0 {
		return fmt.Errorf("negative array size: %d < 0", tmp2)
	}
	t.Players = make([]Player, tmp2)
	for tmp4 := range t.Players {
		if err = binary.Read(rr, binary.BigEndian, t.Players[tmp4].UUID); err != nil {
			return err
		}

		tmp5, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		tmp6 := make([]byte, tmp5)
		tmp7, err := rr.Read(tmp6)
		if err != nil {
			return err
		} else if int64(tmp7) != tmp5 {
			return errors.New("didn't read enough bytes for string")
		}
		t.Players[tmp4].Name = string(tmp6)

		var tmp8 packets.VarInt
		tmp9, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		tmp8 = packets.VarInt(tmp9)

		if tmp8 < 0 {
			return fmt.Errorf("negative array size: %d < 0", tmp8)
		}
		t.Players[tmp4].Properties = make([]Property, tmp8)
		for tmp10 := range t.Players[tmp4].Properties {
			tmp11, err := packets.ReadVarint(rr)
			if err != nil {
				return err
			}
			tmp12 := make([]byte, tmp11)
			tmp13, err := rr.Read(tmp12)
			if err != nil {
				return err
			} else if int64(tmp13) != tmp11 {
				return errors.New("didn't read enough bytes for string")
			}
			t.Players[tmp4].Properties[tmp10].Name = string(tmp12)

			tmp14, err := packets.ReadVarint(rr)
			if err != nil {
				return err
			}
			tmp15 := make([]byte, tmp14)
			tmp16, err := rr.Read(tmp15)
			if err != nil {
				return err
			} else if int64(tmp16) != tmp14 {
				return errors.New("didn't read enough bytes for string")
			}
			t.Players[tmp4].Properties[tmp10].Value = string(tmp15)

			var tmp17 [1]byte
			if _, err = rr.Read(tmp17[:1]); err != nil {
				return err
			}
			t.Players[tmp4].Properties[tmp10].Signed = tmp17[0] == 0x01

			if t.Players[tmp4].Properties[tmp10].Signed {
				tmp18, err := packets.ReadVarint(rr)
				if err != nil {
					return err
				}
				tmp19 := make([]byte, tmp18)
				tmp20, err := rr.Read(tmp19)
				if err != nil {
					return err
				} else if int64(tmp20) != tmp18 {
					return errors.New("didn't read enough bytes for string")
				}
				t.Players[tmp4].Properties[tmp10].Signature = string(tmp19)

			}
		}
		tmp21, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		t.Players[tmp4].Gamemode = packets.VarInt(tmp21)

		tmp22, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		t.Players[tmp4].Ping = packets.VarInt(tmp22)

		var tmp23 [1]byte
		if _, err = rr.Read(tmp23[:1]); err != nil {
			return err
		}
		t.Players[tmp4].HasDisplayName = tmp23[0] == 0x01

		if t.Players[tmp4].HasDisplayName {
			var tmp24 string
			if tmp24, err = packets.ReadString(rr); err != nil {
				return err
			}
			if err = json.Unmarshal([]byte(tmp24), &t.Players[tmp4].DisplayName); err != nil {
				return err
			}
		}
	}
	return
}

func (t *ServerTabComplete) Id() byte {
	return 0x3a // 58
}

func (t *ServerTabComplete) Encode(ww io.Writer) (err error) {
	tmp0 := make([]byte, binary.MaxVarintLen64)
	tmp1 := packets.PutVarint(tmp0, int64(len(t.Matches)))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp1]); err != nil {
		return err
	}

	for tmp2 := range t.Matches {
		tmp3 := make([]byte, binary.MaxVarintLen32)
		tmp4 := []byte(t.Matches[tmp2])
		tmp5 := packets.PutVarint(tmp3, int64(len(tmp4)))
		if err = binary.Write(ww, binary.BigEndian, tmp3[:tmp5]); err != nil {
			return err
		}
		if err = binary.Write(ww, binary.BigEndian, tmp4); err != nil {
			return err
		}

	}
	return
}

func (t *ServerTabComplete) Decode(rr io.Reader) (err error) {
	var tmp0 packets.VarInt
	tmp1, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp0 = packets.VarInt(tmp1)

	if tmp0 < 0 {
		return fmt.Errorf("negative array size: %d < 0", tmp0)
	}
	t.Matches = make([]string, tmp0)
	for tmp2 := range t.Matches {
		tmp3, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		tmp4 := make([]byte, tmp3)
		tmp5, err := rr.Read(tmp4)
		if err != nil {
			return err
		} else if int64(tmp5) != tmp3 {
			return errors.New("didn't read enough bytes for string")
		}
		t.Matches[tmp2] = string(tmp4)

	}
	return
}

func (t *ScoreboardObjective) Id() byte {
	return 0x3b // 59
}

func (t *ScoreboardObjective) Encode(ww io.Writer) (err error) {
	tmp0 := make([]byte, binary.MaxVarintLen32)
	tmp1 := []byte(t.Name)
	tmp2 := packets.PutVarint(tmp0, int64(len(tmp1)))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp2]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp1); err != nil {
		return err
	}

	if err = binary.Write(ww, binary.BigEndian, t.Mode); err != nil {
		return err
	}

	if t.Mode == 0 || t.Mode == 2 {
		tmp3 := make([]byte, binary.MaxVarintLen32)
		tmp4 := []byte(t.Value)
		tmp5 := packets.PutVarint(tmp3, int64(len(tmp4)))
		if err = binary.Write(ww, binary.BigEndian, tmp3[:tmp5]); err != nil {
			return err
		}
		if err = binary.Write(ww, binary.BigEndian, tmp4); err != nil {
			return err
		}

	}
	if t.Mode == 0 || t.Mode == 2 {
		tmp6 := make([]byte, binary.MaxVarintLen32)
		tmp7 := []byte(t.Type)
		tmp8 := packets.PutVarint(tmp6, int64(len(tmp7)))
		if err = binary.Write(ww, binary.BigEndian, tmp6[:tmp8]); err != nil {
			return err
		}
		if err = binary.Write(ww, binary.BigEndian, tmp7); err != nil {
			return err
		}

	}
	return
}

func (t *ScoreboardObjective) Decode(rr io.Reader) (err error) {
	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp1 := make([]byte, tmp0)
	tmp2, err := rr.Read(tmp1)
	if err != nil {
		return err
	} else if int64(tmp2) != tmp0 {
		return errors.New("didn't read enough bytes for string")
	}
	t.Name = string(tmp1)

	if err = binary.Read(rr, binary.BigEndian, t.Mode); err != nil {
		return err
	}

	if t.Mode == 0 || t.Mode == 2 {
		tmp3, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		tmp4 := make([]byte, tmp3)
		tmp5, err := rr.Read(tmp4)
		if err != nil {
			return err
		} else if int64(tmp5) != tmp3 {
			return errors.New("didn't read enough bytes for string")
		}
		t.Value = string(tmp4)

	}
	if t.Mode == 0 || t.Mode == 2 {
		tmp6, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		tmp7 := make([]byte, tmp6)
		tmp8, err := rr.Read(tmp7)
		if err != nil {
			return err
		} else if int64(tmp8) != tmp6 {
			return errors.New("didn't read enough bytes for string")
		}
		t.Type = string(tmp7)

	}
	return
}

func (t *UpdateScore) Id() byte {
	return 0x3c // 60
}

func (t *UpdateScore) Encode(ww io.Writer) (err error) {
	tmp0 := make([]byte, binary.MaxVarintLen32)
	tmp1 := []byte(t.Name)
	tmp2 := packets.PutVarint(tmp0, int64(len(tmp1)))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp2]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp1); err != nil {
		return err
	}

	if err = binary.Write(ww, binary.BigEndian, t.Action); err != nil {
		return err
	}

	tmp3 := make([]byte, binary.MaxVarintLen32)
	tmp4 := []byte(t.ObjectiveName)
	tmp5 := packets.PutVarint(tmp3, int64(len(tmp4)))
	if err = binary.Write(ww, binary.BigEndian, tmp3[:tmp5]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp4); err != nil {
		return err
	}

	if t.Action != 1 {
		tmp6 := make([]byte, binary.MaxVarintLen64)
		tmp7 := packets.PutVarint(tmp6, int64(t.Value))
		if err = binary.Write(ww, binary.BigEndian, tmp6[:tmp7]); err != nil {
			return err
		}

	}
	return
}

func (t *UpdateScore) Decode(rr io.Reader) (err error) {
	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp1 := make([]byte, tmp0)
	tmp2, err := rr.Read(tmp1)
	if err != nil {
		return err
	} else if int64(tmp2) != tmp0 {
		return errors.New("didn't read enough bytes for string")
	}
	t.Name = string(tmp1)

	if err = binary.Read(rr, binary.BigEndian, t.Action); err != nil {
		return err
	}

	tmp3, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp4 := make([]byte, tmp3)
	tmp5, err := rr.Read(tmp4)
	if err != nil {
		return err
	} else if int64(tmp5) != tmp3 {
		return errors.New("didn't read enough bytes for string")
	}
	t.ObjectiveName = string(tmp4)

	if t.Action != 1 {
		tmp6, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		t.Value = packets.VarInt(tmp6)

	}
	return
}

func (t *ShowScoreboard) Id() byte {
	return 0x3d // 61
}

func (t *ShowScoreboard) Encode(ww io.Writer) (err error) {
	if err = binary.Write(ww, binary.BigEndian, t.Position); err != nil {
		return err
	}

	tmp0 := make([]byte, binary.MaxVarintLen32)
	tmp1 := []byte(t.Name)
	tmp2 := packets.PutVarint(tmp0, int64(len(tmp1)))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp2]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp1); err != nil {
		return err
	}

	return
}

func (t *ShowScoreboard) Decode(rr io.Reader) (err error) {
	if err = binary.Read(rr, binary.BigEndian, t.Position); err != nil {
		return err
	}

	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp1 := make([]byte, tmp0)
	tmp2, err := rr.Read(tmp1)
	if err != nil {
		return err
	} else if int64(tmp2) != tmp0 {
		return errors.New("didn't read enough bytes for string")
	}
	t.Name = string(tmp1)

	return
}

func (t *Teams) Id() byte {
	return 0x3e // 62
}

func (t *Teams) Encode(ww io.Writer) (err error) {
	tmp0 := make([]byte, binary.MaxVarintLen32)
	tmp1 := []byte(t.Name)
	tmp2 := packets.PutVarint(tmp0, int64(len(tmp1)))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp2]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp1); err != nil {
		return err
	}

	if err = binary.Write(ww, binary.BigEndian, t.Mode); err != nil {
		return err
	}

	if t.Mode == 0 || t.Mode == 2 {
		tmp3 := make([]byte, binary.MaxVarintLen32)
		tmp4 := []byte(t.Display)
		tmp5 := packets.PutVarint(tmp3, int64(len(tmp4)))
		if err = binary.Write(ww, binary.BigEndian, tmp3[:tmp5]); err != nil {
			return err
		}
		if err = binary.Write(ww, binary.BigEndian, tmp4); err != nil {
			return err
		}

	}
	if t.Mode == 0 || t.Mode == 2 {
		tmp6 := make([]byte, binary.MaxVarintLen32)
		tmp7 := []byte(t.Prefix)
		tmp8 := packets.PutVarint(tmp6, int64(len(tmp7)))
		if err = binary.Write(ww, binary.BigEndian, tmp6[:tmp8]); err != nil {
			return err
		}
		if err = binary.Write(ww, binary.BigEndian, tmp7); err != nil {
			return err
		}

	}
	if t.Mode == 0 || t.Mode == 2 {
		tmp9 := make([]byte, binary.MaxVarintLen32)
		tmp10 := []byte(t.Suffix)
		tmp11 := packets.PutVarint(tmp9, int64(len(tmp10)))
		if err = binary.Write(ww, binary.BigEndian, tmp9[:tmp11]); err != nil {
			return err
		}
		if err = binary.Write(ww, binary.BigEndian, tmp10); err != nil {
			return err
		}

	}
	if t.Mode == 0 || t.Mode == 2 {
		if err = binary.Write(ww, binary.BigEndian, t.FriendlyFire); err != nil {
			return err
		}

	}
	if t.Mode == 0 || t.Mode == 2 {
		tmp12 := make([]byte, binary.MaxVarintLen32)
		tmp13 := []byte(t.NameTagVisibility)
		tmp14 := packets.PutVarint(tmp12, int64(len(tmp13)))
		if err = binary.Write(ww, binary.BigEndian, tmp12[:tmp14]); err != nil {
			return err
		}
		if err = binary.Write(ww, binary.BigEndian, tmp13); err != nil {
			return err
		}

	}
	if t.Mode == 0 || t.Mode == 2 {
		if err = binary.Write(ww, binary.BigEndian, t.Color); err != nil {
			return err
		}

	}
	if t.Mode == 0 || t.Mode == 3 || t.Mode == 4 {
		tmp15 := make([]byte, binary.MaxVarintLen64)
		tmp16 := packets.PutVarint(tmp15, int64(len(t.Players)))
		if err = binary.Write(ww, binary.BigEndian, tmp15[:tmp16]); err != nil {
			return err
		}

		for tmp17 := range t.Players {
			tmp18 := make([]byte, binary.MaxVarintLen32)
			tmp19 := []byte(t.Players[tmp17])
			tmp20 := packets.PutVarint(tmp18, int64(len(tmp19)))
			if err = binary.Write(ww, binary.BigEndian, tmp18[:tmp20]); err != nil {
				return err
			}
			if err = binary.Write(ww, binary.BigEndian, tmp19); err != nil {
				return err
			}

		}
	}
	return
}

func (t *Teams) Decode(rr io.Reader) (err error) {
	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp1 := make([]byte, tmp0)
	tmp2, err := rr.Read(tmp1)
	if err != nil {
		return err
	} else if int64(tmp2) != tmp0 {
		return errors.New("didn't read enough bytes for string")
	}
	t.Name = string(tmp1)

	if err = binary.Read(rr, binary.BigEndian, t.Mode); err != nil {
		return err
	}

	if t.Mode == 0 || t.Mode == 2 {
		tmp3, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		tmp4 := make([]byte, tmp3)
		tmp5, err := rr.Read(tmp4)
		if err != nil {
			return err
		} else if int64(tmp5) != tmp3 {
			return errors.New("didn't read enough bytes for string")
		}
		t.Display = string(tmp4)

	}
	if t.Mode == 0 || t.Mode == 2 {
		tmp6, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		tmp7 := make([]byte, tmp6)
		tmp8, err := rr.Read(tmp7)
		if err != nil {
			return err
		} else if int64(tmp8) != tmp6 {
			return errors.New("didn't read enough bytes for string")
		}
		t.Prefix = string(tmp7)

	}
	if t.Mode == 0 || t.Mode == 2 {
		tmp9, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		tmp10 := make([]byte, tmp9)
		tmp11, err := rr.Read(tmp10)
		if err != nil {
			return err
		} else if int64(tmp11) != tmp9 {
			return errors.New("didn't read enough bytes for string")
		}
		t.Suffix = string(tmp10)

	}
	if t.Mode == 0 || t.Mode == 2 {
		if err = binary.Read(rr, binary.BigEndian, t.FriendlyFire); err != nil {
			return err
		}

	}
	if t.Mode == 0 || t.Mode == 2 {
		tmp12, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		tmp13 := make([]byte, tmp12)
		tmp14, err := rr.Read(tmp13)
		if err != nil {
			return err
		} else if int64(tmp14) != tmp12 {
			return errors.New("didn't read enough bytes for string")
		}
		t.NameTagVisibility = string(tmp13)

	}
	if t.Mode == 0 || t.Mode == 2 {
		if err = binary.Read(rr, binary.BigEndian, t.Color); err != nil {
			return err
		}

	}
	if t.Mode == 0 || t.Mode == 3 || t.Mode == 4 {
		var tmp15 packets.VarInt
		tmp16, err := packets.ReadVarint(rr)
		if err != nil {
			return err
		}
		tmp15 = packets.VarInt(tmp16)

		if tmp15 < 0 {
			return fmt.Errorf("negative array size: %d < 0", tmp15)
		}
		t.Players = make([]string, tmp15)
		for tmp17 := range t.Players {
			tmp18, err := packets.ReadVarint(rr)
			if err != nil {
				return err
			}
			tmp19 := make([]byte, tmp18)
			tmp20, err := rr.Read(tmp19)
			if err != nil {
				return err
			} else if int64(tmp20) != tmp18 {
				return errors.New("didn't read enough bytes for string")
			}
			t.Players[tmp17] = string(tmp19)

		}
	}
	return
}

func (t *ServerPluginMessage) Id() byte {
	return 0x3f // 63
}

func (t *ServerPluginMessage) Encode(ww io.Writer) (err error) {
	tmp0 := make([]byte, binary.MaxVarintLen32)
	tmp1 := []byte(t.Channel)
	tmp2 := packets.PutVarint(tmp0, int64(len(tmp1)))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp2]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp1); err != nil {
		return err
	}

	if _, err = ww.Write(t.Data); err != nil {
		return err
	}
	return
}

func (t *ServerPluginMessage) Decode(rr io.Reader) (err error) {
	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp1 := make([]byte, tmp0)
	tmp2, err := rr.Read(tmp1)
	if err != nil {
		return err
	} else if int64(tmp2) != tmp0 {
		return errors.New("didn't read enough bytes for string")
	}
	t.Channel = string(tmp1)

	if t.Data, err = ioutil.ReadAll(rr); err != nil {
		return
	}
	return
}

func (t *Disconnect) Id() byte {
	return 0x40 // 64
}

func (t *Disconnect) Encode(ww io.Writer) (err error) {
	var tmp0 []byte
	if tmp0, err = json.Marshal(&t.Reason); err != nil {
		return err
	}
	if err = packets.WriteString(ww, string(tmp0)); err != nil {
		return err
	}
	return
}

func (t *Disconnect) Decode(rr io.Reader) (err error) {
	var tmp0 string
	if tmp0, err = packets.ReadString(rr); err != nil {
		return err
	}
	if err = json.Unmarshal([]byte(tmp0), &t.Reason); err != nil {
		return err
	}
	return
}

func (t *SetCompression) Id() byte {
	return 0x46 // 70
}

func (t *SetCompression) Encode(ww io.Writer) (err error) {
	tmp0 := make([]byte, binary.MaxVarintLen64)
	tmp1 := packets.PutVarint(tmp0, int64(t.Threshold))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp1]); err != nil {
		return err
	}

	return
}

func (t *SetCompression) Decode(rr io.Reader) (err error) {
	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	t.Threshold = packets.VarInt(tmp0)

	return
}
