// Generated by protocol_generator; DO NOT EDIT
// protocol_generator -file=login_clientbound.go -direction=clientbound -state=login -package=modern

package modern

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/kurafuto/kyubu/packets"
	"io"
)

func init() {
	packets.Register(packets.Login, packets.ClientBound, 0x00, func() packets.Packet { return &LoginDisconnect{} })
	packets.Register(packets.Login, packets.ClientBound, 0x01, func() packets.Packet { return &EncryptionRequest{} })
	packets.Register(packets.Login, packets.ClientBound, 0x02, func() packets.Packet { return &LoginSuccess{} })
	packets.Register(packets.Login, packets.ClientBound, 0x03, func() packets.Packet { return &SetInitialCompression{} })
}

func (t *LoginDisconnect) Id() byte {
	return 0x00 // 0
}

func (t *LoginDisconnect) Encode(ww io.Writer) (err error) {
	var tmp0 []byte
	if tmp0, err = json.Marshal(&t.Reason); err != nil {
		return err
	}
	if err = packets.WriteString(ww, string(tmp0)); err != nil {
		return err
	}
	return
}

func (t *LoginDisconnect) Decode(rr io.Reader) (err error) {
	var tmp0 string
	if tmp0, err = packets.ReadString(rr); err != nil {
		return err
	}
	if err = json.Unmarshal([]byte(tmp0), &t.Reason); err != nil {
		return err
	}
	return
}

func (t *EncryptionRequest) Id() byte {
	return 0x01 // 1
}

func (t *EncryptionRequest) Encode(ww io.Writer) (err error) {
	tmp0 := make([]byte, binary.MaxVarintLen32)
	tmp1 := []byte(t.ServerID)
	tmp2 := packets.PutVarint(tmp0, int64(len(tmp1)))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp2]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp1); err != nil {
		return err
	}

	tmp3 := make([]byte, binary.MaxVarintLen64)
	tmp4 := packets.PutVarint(tmp3, int64(len(t.PublicKey)))
	if err = binary.Write(ww, binary.BigEndian, tmp3[:tmp4]); err != nil {
		return err
	}

	if _, err = ww.Write(t.PublicKey); err != nil {
		return err
	}
	tmp5 := make([]byte, binary.MaxVarintLen64)
	tmp6 := packets.PutVarint(tmp5, int64(len(t.VerifyToken)))
	if err = binary.Write(ww, binary.BigEndian, tmp5[:tmp6]); err != nil {
		return err
	}

	if _, err = ww.Write(t.VerifyToken); err != nil {
		return err
	}
	return
}

func (t *EncryptionRequest) Decode(rr io.Reader) (err error) {
	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp1 := make([]byte, tmp0)
	tmp2, err := rr.Read(tmp1)
	if err != nil {
		return err
	} else if int64(tmp2) != tmp0 {
		return errors.New("didn't read enough bytes for string")
	}
	t.ServerID = string(tmp1)

	var tmp3 packets.VarInt
	tmp4, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp3 = packets.VarInt(tmp4)

	if tmp3 < 0 {
		return fmt.Errorf("negative array size: %d < 0", tmp3)
	}
	t.PublicKey = make([]byte, tmp3)
	if _, err = rr.Read(t.PublicKey); err != nil {
		return err
	}
	var tmp5 packets.VarInt
	tmp6, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp5 = packets.VarInt(tmp6)

	if tmp5 < 0 {
		return fmt.Errorf("negative array size: %d < 0", tmp5)
	}
	t.VerifyToken = make([]byte, tmp5)
	if _, err = rr.Read(t.VerifyToken); err != nil {
		return err
	}
	return
}

func (t *LoginSuccess) Id() byte {
	return 0x02 // 2
}

func (t *LoginSuccess) Encode(ww io.Writer) (err error) {
	tmp0 := make([]byte, binary.MaxVarintLen32)
	tmp1 := []byte(t.UUID)
	tmp2 := packets.PutVarint(tmp0, int64(len(tmp1)))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp2]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp1); err != nil {
		return err
	}

	tmp3 := make([]byte, binary.MaxVarintLen32)
	tmp4 := []byte(t.Username)
	tmp5 := packets.PutVarint(tmp3, int64(len(tmp4)))
	if err = binary.Write(ww, binary.BigEndian, tmp3[:tmp5]); err != nil {
		return err
	}
	if err = binary.Write(ww, binary.BigEndian, tmp4); err != nil {
		return err
	}

	return
}

func (t *LoginSuccess) Decode(rr io.Reader) (err error) {
	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp1 := make([]byte, tmp0)
	tmp2, err := rr.Read(tmp1)
	if err != nil {
		return err
	} else if int64(tmp2) != tmp0 {
		return errors.New("didn't read enough bytes for string")
	}
	t.UUID = string(tmp1)

	tmp3, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	tmp4 := make([]byte, tmp3)
	tmp5, err := rr.Read(tmp4)
	if err != nil {
		return err
	} else if int64(tmp5) != tmp3 {
		return errors.New("didn't read enough bytes for string")
	}
	t.Username = string(tmp4)

	return
}

func (t *SetInitialCompression) Id() byte {
	return 0x03 // 3
}

func (t *SetInitialCompression) Encode(ww io.Writer) (err error) {
	tmp0 := make([]byte, binary.MaxVarintLen64)
	tmp1 := packets.PutVarint(tmp0, int64(t.Threshold))
	if err = binary.Write(ww, binary.BigEndian, tmp0[:tmp1]); err != nil {
		return err
	}

	return
}

func (t *SetInitialCompression) Decode(rr io.Reader) (err error) {
	tmp0, err := packets.ReadVarint(rr)
	if err != nil {
		return err
	}
	t.Threshold = packets.VarInt(tmp0)

	return
}
